# Database

-   [Basic Mathematics](basic_mathematics.md)

---

## Structured Data

Imagine we want to save our data as a **Table** in a **Raw File**, we must define some rules to **Serialize** and **Unserialize** our data in/from **Raw File**, for example we define `,` character as **Column Seperator**, and `\n` as **Row Seperator** now we can save our data in these structure:

```code
Mohammad,Nemati,23,Iran\n
Zahra,Najafi,21,Iran\n
Shima,Emami,22,Iran
```

Now there are multiple issues:

1. How to **Query** our data
2. How to **Mutate** our data
3. How to **Restructure** our data

The best solution for these problems is to drop **Raw Files** and use a **DBMS** as an interface to interact with **Structured Data**

---

## DBMS (DataBase Management System)

Is a system used to simplify the **Store** and **Load** structured data in **Files** or any types of **Data Source**

![DBMS](assets/dbms.jpg)

Based on the **Storage Type** and **Data Structure Type** and multiple parameters, we have a lot of **Paradigms** in **DBMS** systems, for example:

1. **RDBMS**: Relational DBMS
2. **CDBMS**: Cache DBMS
3. **GDBMS**: Graph DBMS

---

## Database Designing

Now for working with **DBMS** first we must design our data in multiple steps:

> These steps are very similar to **Software Designing** steps

1. **Planning** (Requirement Analysis)
2. **Defining** (Defining Requirements)
3. **Designing** (Designing the Data Structure)
4. **Building** (Implementing Data Structure in DBMS)

> **Example**
>
> We want to design a system for a university
>
> ---
>
> **Planning**: (Contact)
>
> First we must contact with **Users** to find their requirements
>
> 1. Student:
>     - I want to see the units
>     - I want to pick a unit
>     - I want to see unit professor
>     - I want to see professor profile
> 2. Professor:
>     - I want to present unit
>     - I want to see my units
>     - I want to see my units students
>     - I want to set score for my students
>
> ---
>
> **Defining**: (Use Case)
>
> Now we must clearify **Use Cases** and **User Stories** and **Use Case Diagram** from planning step result
>
> ---
>
> **Designing**: (ERD)
>
> In this step we must design our **Data Structure** using standards and diagrams like **ERD** or **UML** or etc
>
> ---
>
> **Building**: (SQL)
>
> In this step we must write our **SQL DDL** commands to build our **Data Structure** in **DBMS** we use
